#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>


#define DEFAULT_COMMAND_SIZE (size_t) 20
#define DEFAULT_ARGUMENT_COUNT 10
#define MAX_NAME_LENGTH 100

char *username;
char *machinename;
int arg_counter;


void process_command(char *command, char **arg_list);

void welcome_screen();

void change_directory(char **args);

void output_redirection(char **args);

int main(int argc, char *argv[]) {

    int return_value;
    int argument_count_coeff = 1;

    char *delimiter = " ";
    char **command;

    username = (char *) malloc(MAX_NAME_LENGTH * sizeof(char));
    machinename = (char *) malloc(MAX_NAME_LENGTH * sizeof(char));
    char *command_line = (char *) malloc(DEFAULT_COMMAND_SIZE);
    size_t *command_size = (size_t *) malloc(sizeof(int));
    command = &command_line;
    argv = (char **) malloc(DEFAULT_ARGUMENT_COUNT * sizeof(char *));

    welcome_screen();
    do {
        arg_counter = 0;
        gethostname(machinename, MAX_NAME_LENGTH);
        getlogin_r(username, MAX_NAME_LENGTH);

        fprintf(stdout, "\n%s@%s $ ", username, machinename);
        return_value = (int) getline(command, command_size, stdin);
        (*command)[return_value - 1] = '\0';

        for (argv[arg_counter] = strtok(*command, delimiter);
             argv[arg_counter] != NULL;
             arg_counter++, argv[arg_counter] = strtok(NULL, delimiter)) {
            if (arg_counter == DEFAULT_ARGUMENT_COUNT - 1)
                argv = realloc(argv, (++argument_count_coeff) * DEFAULT_ARGUMENT_COUNT * sizeof(char *));
        }
        if (strcmp("exit", *command) != 0) {
            process_command(*command, argv);
        }
    } while (strcmp(*command, "exit") != 0);
    fprintf(stdout, "Good Bye!\n");
    return 0;
}


void welcome_screen() {
    fprintf(stdout, "\n\t================================================================================\n");
    fprintf(stdout, "\t                       Simple C Shell\n");
    fprintf(stdout, "\t--------------------------------------------------------------------------------\n");
    fprintf(stdout, "\t             Designed By Mohsen Naghipourfar && Alireza Vezvaei\n");
    fprintf(stdout, "\t================================================================================\n");
    fprintf(stdout, "\n\n");
}

void process_command(char *command, char **arg_list) {
    if (strcmp("cd", command) == 0) {
        change_directory(arg_list);
    }
    else if (arg_counter > 3 && strcmp(">", arg_list[arg_counter - 2]) == 0) {
        output_redirection(arg_list);
    } else {
        pid_t pid;
        pid = fork();
        if (pid == 0) {         // child process
            if (execvp(command, arg_list) == -1)
                fprintf(stdout, "Command not found!\n");
        } else if (pid > 0) {   // parent process
            wait(NULL);
        }
    }
}

void change_directory(char **args) {
    char *home = malloc(strlen("/home/") + strlen(username) + 1);
    strcpy(home, "/home/");
    strcat(home, username);
    if (args[1] == NULL)
        fprintf(stdout, "cd: You have to enter parameter!\n");
    else if ((strcmp(args[1], "~") == 0) || (strcmp(args[1], "~/") == 0))
        chdir(home);
    else if (chdir(args[1]) < 0)
        fprintf(stdout, "cd: %s: No such file or directory\n", args[1]);

}

void output_redirection(char **args) {
    pid_t pid = fork();
    if (pid == 0) {
        int fileDescriptor = open(args[arg_counter - 1], O_CREAT | O_TRUNC | O_WRONLY, 0600);
        dup2(fileDescriptor, STDOUT_FILENO);
        char **argv = (char **) malloc(DEFAULT_ARGUMENT_COUNT * sizeof(char *));
        int i;
        for (i = 0; i < arg_counter - 2; i++) {
            argv[i] = args[i];
        }
        execvp(argv[0], argv);
    } else if (pid > 0) {
        wait(NULL);
    }

}