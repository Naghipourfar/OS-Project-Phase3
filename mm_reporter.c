#include <linux/module.h>    // for all kernel modules
#include <linux/kernel.h>    // for KERN_INFO
#include <linux/init.h>      // for __init and __exit macros
#include <linux/sched/signal.h>
#include <linux/sched.h>     // for_each_process()
#include <linux/proc_fs.h>
#include <linux/kthread.h>
#include <linux/delay.h>



// Designed by Mohsen naghipourfar && Alireza Vezvaei

MODULE_LICENSE("Mohsen/Alireza");       // set licence of module
MODULE_AUTHOR("Mohsen Naghipourfar && Alireza Vezvaei");    // authors of modules
MODULE_DESCRIPTION("A Module which just says hello to the OS!");    // module description
static struct task_struct *thread_reporter;
static pid_t thread_id;

int report_memory_change(void *unused_data)
{
	allow_signal(SIGKILL);
	int prev[2000] = {0};
	while(!kthread_should_stop()){
		struct task_struct* task;
       		for_each_process(task) {
               	        if(task->mm != NULL && task->pid < 2000){
				int diff = task->mm->total_vm - prev[task->pid];
				prev[task->pid] = task->mm->total_vm;
				if (diff > 0)                      		 
					printk(KERN_INFO "Allocated %d for PID = %d\n", diff, task->pid);
				else if (diff < 0)
					printk(KERN_INFO "Deallocated %d from PID = %d\n", -1*diff, task->pid);
        	        }
	    	}
		ssleep(5);
		if (signal_pending(thread_reporter)){
			printk(KERN_INFO "The SIGKILL has been sent to thread successfully!");
            		break;
		}
	}
	printk(KERN_INFO "Thread's work is done!");
        return 0;
}


static int __init mm_reporter_init(void) {     // init function for hello_os module
    	printk(KERN_INFO "Hello Dear OS! This is a module created by Mohsen && Alireza! :)) \n");
	//kthread_create(&report_memory_change, NULL, "Mohsen/Alireza");
        thread_reporter = kthread_run(&report_memory_change, NULL, "Mohsen/Alireza");

	if (thread_reporter){
		thread_id = thread_reporter->pid;
		printk(KERN_INFO "Thread has been created successfully!");
	}
	else {
		thread_id = 0;
		printk(KERN_INFO "Thread hasn't been created!");
	}
 	return 0;    // Non-zero return means that the module couldn't be loaded.
}

static void __exit mm_reporter_cleanup(void) { // cleanup function for hello_os module
	kthread_stop(thread_reporter);
	//kill(thread_id, SIGKILL);
	printk(KERN_INFO "GoodBye OS! You've removed our module from your kernel! :(( \n");
}

module_init(mm_reporter_init);     // define init funciton for module
module_exit(mm_reporter_cleanup);  // define cleanup function for module
